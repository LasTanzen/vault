import { VaultPage } from './app.po';

describe('vault App', () => {
  let page: VaultPage;

  beforeEach(() => {
    page = new VaultPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
