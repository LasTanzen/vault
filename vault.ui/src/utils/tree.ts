export class TreeNode<T> {

  get depth(): number {
    return this.calculateDepth(this);
  }

  parent: TreeNode<T>;
  children: TreeNode<T>[] = [];

  constructor(public data: T) {}

  add(node: TreeNode<T>): TreeNode<T> {
    node.parent = this;
    this.children.push(node);
    return this;
  }

  remove(): TreeNode<T> {
    if (this.parent) {
      const index = this.parent.children.indexOf(this);
      this.parent.children.splice(index, 1);
      this.parent = null;
    } else {
      throw new Error("Impossible to remove node without parent.");
    }
    return this;
  }

  private calculateDepth(node: TreeNode<T>, count: number = 0): number {
    return !node.parent ? count : this.calculateDepth(node.parent, count + 1);
  };

}

export class Tree<T> {

  constructor(public root: TreeNode<T> = null) {}

  traverseDepthFirst(callback: (node: TreeNode<T>) => void) {
    (function recurse(node: TreeNode<T>) {
      node.children.forEach(childNode => recurse(childNode));
      callback(node);
    })(this.root);
  }

  traverseBreathFirst(callback: (node: TreeNode<T>) => void) {
    const queue: TreeNode<T>[] = [];
    let node: TreeNode<T> = this.root;

    while(!!node) {
      node.children.forEach(childNode => queue.push(childNode))
      callback(node);
      node = queue.shift();
    }
  }

}
