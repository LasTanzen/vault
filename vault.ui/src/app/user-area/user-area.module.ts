import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserAreaRoutingModule } from './user-area-routing.module';
import { UserAreaComponent } from './user-area.component';

@NgModule({
  imports: [
    CommonModule,
    UserAreaRoutingModule
  ],
  declarations: [UserAreaComponent]
})
export class UserAreaModule { }
