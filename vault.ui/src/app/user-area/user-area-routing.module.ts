import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserAreaComponent } from './user-area.component';

const routes: Routes = [
  {
    path: '',
    component: UserAreaComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'files'
      },
      {
        path: 'files',
        loadChildren: '../file-structure/file-structure.module#FileStructureModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAreaRoutingModule { }
