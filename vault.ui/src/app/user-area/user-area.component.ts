import { Component } from '@angular/core';

@Component({
  selector: 'app-user-area',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class UserAreaComponent {
  constructor() { }
}
