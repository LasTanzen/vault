import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as AuthActions from '../auth/auth.actions';
import { AppState } from '../app-state';

@Component({
  selector: 'app-header',
  template: `
    <nav>
      <div class="nav-wrapper">
        <div class="container">
          <div class="right" *ngIf="authenticated$ | async">
            Hi {{ userName$ | async }}
            <button class="waves-effect waves-light btn" (click)="logout()">Log out</button>
          </div>
        </div>
      </div>
    </nav>
  `,
  styles: [`
    button {
      margin-left: 15px;
    }
  `]
})
export class HeaderComponent {

  authenticated$: Observable<boolean> = this.store.select(state => state.auth.authenticated);
  userName$: Observable<string> = this.store.select(state => state.auth.user)
    .filter(user => user !== undefined).map(user => user.name);

  constructor(private store: Store<AppState>, private router: Router) { }

  logout() {
    this.store.dispatch(new AuthActions.Invalidate());
    this.router.navigate(['login']);
  }

}
