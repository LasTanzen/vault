import { compose } from '@ngrx/core';
import { StoreModule, combineReducers } from '@ngrx/store';

import { authReducer } from './auth';
import { fileStructureReducer } from './file-structure';

export const reducers = {
  auth: authReducer,
  fileStructure: fileStructureReducer
};

export const store = StoreModule.provideStore(reducers);
