import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { store } from './store';
import { AuthModule } from './auth';
import { GuardsModule } from './guards';
import { LoginModule } from './login';
import { NotFoundComponent } from './not-found.component';
import { HeaderModule } from './header';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    GuardsModule,
    LoginModule,
    HeaderModule,
    store,
    StoreDevtoolsModule.instrumentOnlyWithExtension()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
