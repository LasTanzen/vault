import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UserAreaGuard } from './user-area.guard';
import { store } from '../store';

describe('UserAreaGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, store],
      providers: [UserAreaGuard]
    });
  });

  it('should ...', inject([UserAreaGuard], (guard: UserAreaGuard) => {
    expect(guard).toBeTruthy();
  }));
});
