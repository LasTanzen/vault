import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { AppState } from '../app-state';

@Injectable()
export class UserAreaGuard implements CanActivate {
  constructor(private router: Router, private store: Store<AppState>) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.store.select(state => state.auth.authenticated).first()
    .do(authenticated => !authenticated && this.router.navigate(['login']));
  }
}
