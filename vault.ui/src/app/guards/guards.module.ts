import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginGuard } from './login.guard';
import { RouterModule } from '@angular/router';
import { UserAreaGuard } from './user-area.guard';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    LoginGuard,
    UserAreaGuard
  ]
})
export class GuardsModule { }
