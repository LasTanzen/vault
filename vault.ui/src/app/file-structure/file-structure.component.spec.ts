import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { FileStructureComponent } from './file-structure.component';
import { FolderComponent } from './folder.component';
import { store } from '../store';

describe('FileStructureComponent', () => {
  let component: FileStructureComponent;
  let fixture: ComponentFixture<FileStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, store ],
      declarations: [ FileStructureComponent, FolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
