import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { TreeNode, Tree } from '../../utils/tree';
import { FileObject } from './file-object';
import { AppState } from '../app-state';
import * as FileStructureActions from './file-structure.actions';

@Component({
  selector: 'app-file-structure',
  template: `
    <app-folder [folder]="folder$ | async" *ngIf="show$ | async"></app-folder>
  `,
  styles: []
})
export class FileStructureComponent implements OnInit {

  folder$: Observable<TreeNode<FileObject>> = this.store.select(state => state.fileStructure.selected);
  show$: Observable<boolean> = this.store.select(state => state.fileStructure.selected !== undefined);

  constructor(private route: ActivatedRoute, private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new FileStructureActions.LoadFileStructure());
    this.route.queryParamMap.subscribe(params => this.parseQueryParams(params));
  }

  private parseQueryParams(params: ParamMap) {
    if (params.has('folder')) {
      this.store.dispatch(new FileStructureActions.SelectFolder(params.get('folder')));
    } else {
      this.store.dispatch(new FileStructureActions.SelectFolder());
    }
  }

}
