import { Tree, TreeNode } from '../../utils/tree';
import { FileObject } from './file-object';

export interface FileStructureState {
  loading: boolean,
  loadingFailed: boolean,
  loadingFailReason?: string,
  fileTree?: Tree<FileObject>,
  selected?: TreeNode<FileObject>
}
