import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as FileStructureActions from './file-structure.actions';
import { AppState } from '../app-state';

import { createFileTreeMock } from './file-structure-mock';

@Injectable()
export class FileStructureEffects {constructor(private actions$: Actions) { }

  @Effect() load$ = this.actions$
  .ofType(FileStructureActions.LOAD_FILE_STRUCTURE)
  .map(() => (createFileTreeMock())) // mock api call
  .map(tree => new FileStructureActions.LoadFileStructureSuccess(tree))
  .catch(error => Observable.of(new FileStructureActions.LoadFileStructureFail(error)));

}
