import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FileStructureComponent } from './file-structure.component';

const routes: Routes = [{
  path: '',
  component: FileStructureComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileStructureRoutingModule { }
