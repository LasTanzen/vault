import { FileStructureState } from './file-structure-state';
import * as Actions from './file-structure.actions';

import { TreeNode } from '../../utils/tree';
import { FileObject } from './file-object';

export type Action = Actions.All;

export const initial: FileStructureState = {
  loading: false,
  loadingFailed: false
};

export function fileStructureReducer(state: FileStructureState = initial, action: Action): FileStructureState {
  switch (action.type) {
    case Actions.LOAD_FILE_STRUCTURE: {
      delete state.loadingFailReason;
      return { ...state, loading: true, loadingFailed: false };
    }
    case Actions.LOAD_FILE_STRUCTURE_SUCCESS: {
      delete state.loadingFailReason;
      return { ...state, loading: false, loadingFailed: false, fileTree: action.payload };
    }
    case Actions.LOAD_FILE_STRUCTURE_FAIL: {
      return { ...state, loading: false, loadingFailed: true, loadingFailReason: action.payload };
    }
    case Actions.SELECT_FOLDER: {
      let selected: TreeNode<FileObject>;
      if (state.fileTree === undefined) {
        return { ...state };
      } else if (action.payload) {
        state.fileTree.traverseDepthFirst(node => 
          selected = node.data.uuid === action.payload ? node : selected);
      } else {
        selected = state.fileTree.root;
      }
      return { ...state, selected };
    }
    default: {
      return state;
    }
  }
}
