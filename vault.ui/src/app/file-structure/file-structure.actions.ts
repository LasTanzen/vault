import { Action } from '@ngrx/store';
import { Tree } from '../../utils/tree';
import { FileObject } from './file-object';

export const LOAD_FILE_STRUCTURE = '[FileStructure] Load File Structure';
export const LOAD_FILE_STRUCTURE_SUCCESS = '[FileStructure] Load File Structure Success';
export const LOAD_FILE_STRUCTURE_FAIL = '[FileStructure] Load File Structure Fail';
export const SELECT_FOLDER = '[FileStructure] Select Folder';

export class LoadFileStructure implements Action {
  readonly type = LOAD_FILE_STRUCTURE;
}

export class LoadFileStructureSuccess implements Action {
  readonly type = LOAD_FILE_STRUCTURE_SUCCESS;
  constructor(public payload: Tree<FileObject>) {}
}

export class LoadFileStructureFail implements Action {
  readonly type = LOAD_FILE_STRUCTURE_FAIL;
  constructor(public payload: string) {}
}

export class SelectFolder implements Action {
  readonly type = SELECT_FOLDER;
  constructor(public payload?: string) {}
}

export type All
  = LoadFileStructure
  | LoadFileStructureSuccess
  | LoadFileStructureFail
  | SelectFolder
