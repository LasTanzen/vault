import { initial, fileStructureReducer } from './file-structure.reducer';
import { FileStructureState } from './file-structure-state';
import * as Actions from './file-structure.actions';
import { FileObject } from './file-object';
import { createFileTreeMock } from './file-structure-mock'
import { TreeNode } from '../../utils/tree'

describe('fileStructureReducer', () => {

  const loading: FileStructureState = { ...initial, loading: true, loadingFailed: false };
  const loadingSuccess: FileStructureState = { ...initial, loading: false, loadingFailed: false,
    fileTree: createFileTreeMock() };
  const loadingFailed: FileStructureState = { ...initial, loading: false, loadingFailed: true, 
    loadingFailReason: 'No permission' };

  it('loads the structure with LoadFileStructure', () => {
    let state = fileStructureReducer(undefined, new Actions.LoadFileStructure());
    expect(state).toEqual(loading);
  });

  it('sets the file tree with LoadFileStructureSuccess', () => {
    let state = fileStructureReducer(undefined, new Actions.LoadFileStructure());
    state = fileStructureReducer(state, new Actions.LoadFileStructureSuccess(createFileTreeMock()));
    expect(state).toEqual(loadingSuccess);
  });

  it('sets the loading state to failed with LoadFileStructureFail', () => {
    let state = fileStructureReducer(undefined, new Actions.LoadFileStructure());
    state = fileStructureReducer(state, new Actions.LoadFileStructureFail('No permission'));
    expect(state).toEqual(loadingFailed);
  });

  it('sets the selected tree node with SelectFolder', () => {
    const mock = createFileTreeMock();

    let state = fileStructureReducer(undefined, new Actions.LoadFileStructure());
    state = fileStructureReducer(state, new Actions.LoadFileStructureSuccess(mock));
    state = fileStructureReducer(state, new Actions.SelectFolder('3'));

    let selectedNode: TreeNode<FileObject>;
    mock.traverseDepthFirst(node => selectedNode = node.data.uuid === '3' ? node : selectedNode);

    expect(state.selected).toEqual(selectedNode);

    state = fileStructureReducer(state, new Actions.SelectFolder());

    expect(state.selected).toEqual(mock.root);

  });

});
