import { Component, OnChanges, ChangeDetectionStrategy, Input, SimpleChanges } from '@angular/core';

import { TreeNode } from '../../utils/tree';
import { FileObject } from './file-object'
import { FileObjectTypes } from './file-object-types.enum'

@Component({
  selector: 'app-folder',
  template: `
    <h3>Files</h3>
    <div class="collection">
      <a *ngIf="!isRoot" class="collection-item"
        [routerLink]="" [queryParams]="{ folder: folder.parent.data.uuid }">..</a>
      <a *ngFor="let folder of folders" class="collection-item" 
        [routerLink]="" [queryParams]="{ folder: folder.data.uuid }">
        <i class="material-icons">folder</i>{{ folder.data.name }}
      </a>
      <a *ngFor="let file of files" class="collection-item">
        <i class="material-icons">insert_drive_file</i>{{ file.name }}
      </a>
    </div>
  `,
  styles: [`
    .collection a.collection-item i {
      color: grey;
      margin-right: 15px;
    }
    .collection a.collection-item {
      align-items: center;
      display: flex;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FolderComponent implements OnChanges {

  @Input() folder: TreeNode<FileObject>;

  files: FileObject[];
  folders: TreeNode<FileObject>[];
  isRoot: boolean = true;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.folder !== undefined) {
      this.isRoot = !this.folder.parent;

      this.files = this.folder.children.map(node => node.data)
        .filter(obj => obj.type === FileObjectTypes.DefaultFile);

      this.folders = this.folder.children.filter(node => node.data.type === FileObjectTypes.Folder);
    } 
  }

}
