import { Tree, TreeNode } from '../../utils/tree';
import { FileObject } from './file-object';
import { FileObjectTypes } from './file-object-types.enum';

export function createFileTreeMock() {
  const root = new TreeNode<FileObject>({
    uuid: '1',
    name: '',
    type: FileObjectTypes.Folder
  });

  const subfolder1 = new TreeNode<FileObject>({
    uuid: '2',
    name: 'subfolder1',
    type: FileObjectTypes.Folder
  });

  const subfolder2 = new TreeNode<FileObject>({
    uuid: '3',
    name: 'subfolder2',
    type: FileObjectTypes.Folder
  });

  const rootFile1 = new TreeNode<FileObject>({
    uuid: '4',
    name: 'rootFile1',
    type: FileObjectTypes.DefaultFile
  });

  const rootFile2 = new TreeNode<FileObject>({
    uuid: '5',
    name: 'rootFile2',
    type: FileObjectTypes.DefaultFile
  });
  const rootFile3 = new TreeNode<FileObject>({
    uuid: '6',
    name: 'rootFile3',
    type: FileObjectTypes.DefaultFile
  });

  const subfolderFile1 = new TreeNode<FileObject>({
    uuid: '7',
    name: 'subfolder1File',
    type: FileObjectTypes.DefaultFile
  });

  const subfolderFile2 = new TreeNode<FileObject>({
    uuid: '8',
    name: 'subfolder2File',
    type: FileObjectTypes.DefaultFile
  });

  const subsubfolder = new TreeNode<FileObject>({
    uuid: '9',
    name: 'subsubfolder',
    type: FileObjectTypes.Folder
  });

  root.add(subfolder1).add(subfolder2).add(rootFile1).add(rootFile2).add(rootFile3);
  subfolder1.add(subfolderFile1).add(subfolderFile2).add(subsubfolder);

  return new Tree<FileObject>(root);
}
