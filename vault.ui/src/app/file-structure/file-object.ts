import { FileObjectTypes } from './file-object-types.enum';

export interface FileObject {
  uuid: string,
  name: string,
  type: FileObjectTypes
}
