import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import { FileStructureRoutingModule } from './file-structure-routing.module';
import { FileStructureComponent } from './file-structure.component';
import { FolderComponent } from './folder.component';
import { FileStructureEffects } from './file-structure.effects';

@NgModule({
  imports: [
    CommonModule,
    FileStructureRoutingModule,
    EffectsModule.run(FileStructureEffects)
  ],
  declarations: [FileStructureComponent, FolderComponent]
})
export class FileStructureModule { }
