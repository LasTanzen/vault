import { async, TestBed, inject } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';

import { store } from '../store';
import { FileStructureEffects } from './file-structure.effects';
import { AppState } from '../app-state';
import * as FileStructureActions from './file-structure.actions';
import { createFileTreeMock } from './file-structure-mock';

describe('GlobalAnalysisSettingsEffects', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ store, EffectsTestingModule, HttpModule ],
    providers: [ 
      FileStructureEffects,
      {
        provide: Http,
        useFactory: (mockBackend, options) => {
          return new Http(mockBackend, options);
        },
        deps: [MockBackend, BaseRequestOptions]
      },
      MockBackend,
      BaseRequestOptions
    ]
  }));

  let runner: EffectsRunner;
  let fileStructureEffects: FileStructureEffects;

  beforeEach(inject([EffectsRunner, FileStructureEffects], (_runner, _effects) => {
    runner = _runner;
    fileStructureEffects = _effects;
  }));

  it('should return a LOAD_FILE_STRUCTURE_SUCCESS action after loading', 
  async(inject([Store], (store: Store<AppState>) => {

    runner.queue(new FileStructureActions.LoadFileStructure());

    fileStructureEffects.load$.subscribe(result => {
      expect(result).toEqual(new FileStructureActions.LoadFileStructureSuccess(createFileTreeMock()));
    });
  })));
  
});
