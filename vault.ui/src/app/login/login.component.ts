import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as AuthActions from '../auth/auth.actions';
import { AppState } from '../app-state';

@Component({
  selector: 'app-login',
  template: `
    <h3>Login</h3>
    <div class="input-field col s6">
      <input placeholder="Name" type="text" class="validate" [formControl]="nameControl" 
        [class.invalid]="!nameControl.valid">
    </div>
    <button (click)="submit()" [disabled]="!nameControl.valid" 
      class="waves-effect waves-light btn-large">Button</button>
  `,
  styles: []
})
export class LoginComponent implements OnInit {

  nameControl = new FormControl('', Validators.required);

  constructor(private router: Router, private store: Store<AppState>) { }

  ngOnInit() {
    this.nameControl.valueChanges.debounceTime(200)
    .subscribe(name => this.store.dispatch(new AuthActions.SetCredentials({ name })));
  }

  submit() {
    this.store.dispatch(new AuthActions.Authenticate());
    this.store.select(state => state.auth.authenticated).first()
    .filter(authenticated => authenticated).subscribe(() => this.router.navigate(['/']));
  }

}
