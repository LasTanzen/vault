import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginGuard, UserAreaGuard } from './guards';
import { LoginComponent } from './login';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [ LoginGuard ]
  },
  {
    path: '',
    loadChildren: './user-area/user-area.module#UserAreaModule',
    canActivate: [ UserAreaGuard ]
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
