import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import { AuthEffects } from './auth.effects';

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.run(AuthEffects)
  ],
  declarations: []
})
export class AuthModule { }
