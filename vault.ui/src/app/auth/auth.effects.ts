import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as AuthActions from './auth.actions';
import { AppState } from '../app-state';

@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions,
    private store: Store<AppState>) { }

  @Effect() authenticate$ = this.actions$
  .ofType(AuthActions.AUTHENTICATE)
  .switchMap(() => this.store.select(state => state.auth.credentials).first())
  .map(credentials => ({ name: credentials.name })) // mock api call
  .map(user => new AuthActions.AuthenticateSuccess(user))
  .catch(error => Observable.of(new AuthActions.AuthenticateFail(error)));

}
