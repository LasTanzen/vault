import { Action } from '@ngrx/store';
import { User } from './user';
import { Credentials } from './credentials'

export const SET_CREDENTIALS = '[Auth] Set Credentials';
export const AUTHENTICATE = '[Auth] Authenticate';
export const AUTHENTICATE_SUCCESS = '[Auth] Authenticate Success';
export const AUTHENTICATE_FAIL = '[Auth] Authenticate Fail';
export const INVALIDATE = '[Auth] Invalidate';

export class SetCredentials implements Action {
  readonly type = SET_CREDENTIALS;
  constructor(public payload: Credentials) {}
}

export class Authenticate implements Action {
  readonly type = AUTHENTICATE;
  constructor() {}
}

export class AuthenticateSuccess implements Action {
  readonly type = AUTHENTICATE_SUCCESS;
  constructor(public payload: User) {}
}

export class AuthenticateFail implements Action {
  readonly type = AUTHENTICATE_FAIL;
  constructor(public payload: string) {}
}

export class Invalidate implements Action {
  readonly type = INVALIDATE;
}

export type All
  = SetCredentials
  | Authenticate
  | AuthenticateSuccess
  | AuthenticateFail
  | Invalidate
