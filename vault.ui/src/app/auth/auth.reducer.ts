import { AuthState } from './auth-state';
import * as Actions from './auth.actions';

export type Action = Actions.All;

export const initial: AuthState = {
  authenticated: false,
  authenticating: false,
  authenticationFailed: false
};

export function authReducer(state: AuthState = initial, action: Action): AuthState {
  switch (action.type) {
    case Actions.SET_CREDENTIALS: {
      return { ...state, credentials: action.payload };
    }
    case Actions.AUTHENTICATE: {
      delete state.user;
      return { ...state, authenticated: false, authenticating: true, authenticationFailed: false };
    }
    case Actions.AUTHENTICATE_SUCCESS: {
      delete state.credentials;
      return { ...state, user: { ...action.payload }, authenticated: true, authenticating: false };
    }
    case Actions.AUTHENTICATE_FAIL: {
      delete state.user;
      return { ...state, authenticated: false, authenticating: false, authenticationFailed: true, 
        authenticationFailReason: action.payload }
    }
    case Actions.INVALIDATE: {
      return { ...initial };
    }
    default: {
      return state;
    }
  }
}
