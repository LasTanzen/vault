import { initial, authReducer } from './auth.reducer';
import { AuthState } from './auth-state';
import * as Actions from './auth.actions';
import { User } from './user';
import { Credentials } from './credentials';

describe('authReducer', () => {

  const userMock: User = {
    name: 'foo'
  }
  const credentialsMock: Credentials = {
    name: 'foofoo'
  }

  const withCredentials: AuthState = { ...initial, credentials: credentialsMock };
  const authenticating: AuthState = { ...initial, 
    authenticated: false, authenticating: true, authenticationFailed: false, credentials: credentialsMock };
  const authenticated: AuthState = { ...initial, 
    authenticated: true, authenticating: false, authenticationFailed: false, user: userMock };
  const authenticationFailed: AuthState = { ...initial,
    authenticated: false, authenticating: false, authenticationFailed: true, authenticationFailReason: 'bar',
    credentials: credentialsMock
  };

  it('sets the credentials with SetCredentials', () => {
    let state = authReducer(undefined, new Actions.SetCredentials({ ...credentialsMock }));
    expect(state).toEqual(withCredentials);
  });

  it('sets state to authentication with Authenticate', () => {
    let state = authReducer(undefined, new Actions.SetCredentials({ ...credentialsMock }));
    state = authReducer(state, new Actions.Authenticate());
    expect(state).toEqual(authenticating);
  });

  it('sets state to authenticated and sets user object with AuthenticateSuccess', () => {
    let state = authReducer(undefined, new Actions.Authenticate());
    state = authReducer(state, new Actions.AuthenticateSuccess({ ...userMock }));
    expect(state).toEqual(authenticated);
  });

  it('sets state to not authentication failed and the given reason with AuthenticateFail', () => {
    let state = authReducer(undefined, new Actions.SetCredentials({ ...credentialsMock }));
    state = authReducer(state, new Actions.Authenticate());
    state = authReducer(state, new Actions.AuthenticateFail('bar'))
    expect(state).toEqual(authenticationFailed);
  });

  it('resets the state with Invalidate', () => {
    let state = authReducer(undefined, new Actions.Authenticate());
    state = authReducer(state, new Actions.AuthenticateSuccess({ ...userMock }));
    state = authReducer(state, new Actions.Invalidate());
    expect(state).not.toEqual(authenticated);
    expect(state).toEqual({ ...initial });
  });

});
