import { User } from './user';
import { Credentials } from './credentials';

export interface AuthState {
  authenticated: boolean,
  authenticating: boolean,
  authenticationFailed: boolean,
  authenticationFailReason?: string,
  credentials?: Credentials,
  user?: User
}
