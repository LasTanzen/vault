import { async, TestBed, inject } from '@angular/core/testing';
import { HttpModule, Http, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';

import { store } from '../store';
import { AuthEffects } from './auth.effects';
import { AppState } from '../app-state';
import * as AuthActions from './auth.actions';

describe('GlobalAnalysisSettingsEffects', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ store, EffectsTestingModule, HttpModule ],
    providers: [ 
      AuthEffects,
      {
        provide: Http,
        useFactory: (mockBackend, options) => {
          return new Http(mockBackend, options);
        },
        deps: [MockBackend, BaseRequestOptions]
      },
      MockBackend,
      BaseRequestOptions
    ]
  }));

  let runner: EffectsRunner;
  let authEffects: AuthEffects;

  beforeEach(inject([EffectsRunner, AuthEffects], (_runner, _effects) => {
    runner = _runner;
    authEffects = _effects;
  }));

  it('should return an AUTHENTICATE_SUCCESS action after authenticating', 
  async(inject([Store], (store: Store<AppState>) => {

    store.dispatch(new AuthActions.SetCredentials({ name: 'foo' }));
    runner.queue(new AuthActions.Authenticate());

    authEffects.authenticate$.subscribe(result => {
      expect(result).toEqual(new AuthActions.AuthenticateSuccess({ name: 'foo' }));
    });
  })));
  
});
