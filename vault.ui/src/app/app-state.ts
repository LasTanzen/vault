import { AuthState } from './auth';
import { FileStructureState } from './file-structure';

export interface AppState {
  auth: AuthState,
  fileStructure: FileStructureState
}
